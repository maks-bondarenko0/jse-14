package ru.t1.bondarenko.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void removeTaskByID();

    void removeTaskByIndex();

    void showTaskByID();

    void showTaskByIndex();

    void showTaskByProjectID();

    void updateTaskByID();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
