package ru.t1.bondarenko.tm.api.repository;

import ru.t1.bondarenko.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(Project project);

    void deleteAll();

    boolean existsById(String id);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project delete(Project project);

    Project removeByID(String id);

    Project removeByIndex(Integer index);

}
