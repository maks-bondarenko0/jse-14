package ru.t1.bondarenko.tm.command.system;

public class ApplicationDeveloperInfoCommand extends AbstractSystemCommand {

    public final static String DESCRIPTION = "Display developer info.";

    public final static String NAME = "info";

    public final static String ARGUMENT = "-i";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DEVELOPER INFORMATION]");
        System.out.println("bondarenko Ivan");
        System.out.println("ivanbondarenko@gmail.com");
    }
}
