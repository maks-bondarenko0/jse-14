package ru.t1.bondarenko.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Delete all Tasks.";

    public final static String NAME = "task-delete";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE TASKS]");
        getTaskService().deleteAll();
        System.out.println("[TASKS DELETED]");
    }
}
