package ru.t1.bondarenko.tm.exception;

public abstract class AbstractExtension extends RuntimeException {

    public AbstractExtension() {
    }

    public AbstractExtension(String message) {
        super(message);
    }

    public AbstractExtension(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractExtension(Throwable cause) {
        super(cause);
    }

    public AbstractExtension(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
