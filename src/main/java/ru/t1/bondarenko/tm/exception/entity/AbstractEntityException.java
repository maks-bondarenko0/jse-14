package ru.t1.bondarenko.tm.exception.entity;

import ru.t1.bondarenko.tm.exception.AbstractExtension;

public class AbstractEntityException extends AbstractExtension {

    public AbstractEntityException() {
    }

    public AbstractEntityException(String message) {
        super(message);
    }

    public AbstractEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
