package ru.t1.bondarenko.tm.exception.entity;

public class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
