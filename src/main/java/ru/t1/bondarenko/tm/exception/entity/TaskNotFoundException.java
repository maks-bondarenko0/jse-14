package ru.t1.bondarenko.tm.exception.entity;

public class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
