package ru.t1.bondarenko.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String message) {
        super("Error! Command \"" + message + "\" not supported...");
    }

}
